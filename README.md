# book-storage-backend

---

## development

Use [poetry](https://python-poetry.org/) to manage the development environment.

```bash
poetry install
```