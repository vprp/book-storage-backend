"""Module.

(c) Raise Partner 2022
Vincent Pfister
"""
from unittest.mock import MagicMock, Mock


def mock_abstract(abstract_class):
    """
    >>> import abc
    >>> class Abstract(metaclass=abc.ABCMeta):
    ...     @abc.abstractmethod
    ...     def bar(self):
    ...        return None

    >>> c = mock_abstract(Abstract)
    >>> c.__name__
    'MockConcreteAbstract'
    >>> c().bar() # doctest: +ELLIPSIS
    <MagicMock name='bar()' id='...'>
    """
    if "__abstractmethods__" not in abstract_class.__dict__:
        return abstract_class
    assert hasattr(abstract_class, "__abstractmethods__")
    new_dict = abstract_class.__dict__.copy()
    for abstract_method in abstract_class.__abstractmethods__:
        # replace each abc method or property with an identity function:
        new_dict[abstract_method] = MagicMock(name=abstract_method)
        # new_dict[abstract_method] = lambda x, *args, **kw: (x, args, kw)
    # creates a new class, with the overriden ABCs:
    return type(f"MockConcrete{abstract_class.__name__}", (abstract_class,), new_dict)


def mock_abstract_generic(abstract_generic_class, name=""):
    """
    >>> from abc import ABC, abstractmethod
    >>> from typing import TypeVar, Generic
    >>> T = TypeVar("T")
    >>> class Abstract(ABC, Generic[T]):
    ...     @abstractmethod
    ...     def bar(self) -> T:
    ...        pass

    >>> c = mock_abstract_generic(Abstract[int])
    >>> c.__name__
    'MockConcreteAbstract'
    >>> c().bar() # doctest: +ELLIPSIS
    <MagicMock name='bar()' id='...'>
    """
    if "__origin__" not in abstract_generic_class.__dict__:
        raise TypeError("must be a generic class")
    # return mock_abstract(abstract_generic_class.__origin__)
    abstract_class = abstract_generic_class.__origin__
    if "__abstractmethods__" not in abstract_class.__dict__:
        return abstract_class
    assert hasattr(abstract_class, "__abstractmethods__")
    new_dict = abstract_class.__dict__.copy()
    for abstract_method in abstract_class.__abstractmethods__:
        # replace each abc method or property with an identity function:
        new_dict[abstract_method] = MagicMock(name=abstract_method)
        # new_dict[abstract_method] = lambda x, *args, **kw: (x, args, kw)
    # creates a new class, with the overriden ABCs:
    new_name = f"MockConcrete{abstract_class.__name__}" + f"_{name}" if name else ""
    return type(new_name, (abstract_class,), new_dict)


def reset_inner_mocks(obj):
    for name in dir(obj):
        try:
            attr = getattr(obj, name)
            if isinstance(attr, Mock):
                attr.reset_mock()
        except AttributeError:
            pass
