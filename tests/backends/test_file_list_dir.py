"""Unit tests for ...

(c) Raise Partner 2022
Vincent Pfister
"""
from dataclasses import dataclass

from _pytest.fixtures import fixture
from assertpy import assert_that  # type: ignore[import]

from book_storage_backend.backends.file_tools import FileListDir
from book_storage_backend.backends.key_encoder import IntKeyEncoder


@dataclass
class Family:
    name: str


@dataclass
class Child:
    name: str


class TestFileListDir:
    """Unit tests for the FileListDir class."""

    @fixture()
    def key_encoder(self):
        return IntKeyEncoder("person", "json")

    @fixture()
    def list_dir(self, tmp_path, key_encoder) -> FileListDir[Family, int, Child]:
        return FileListDir(tmp_path, Family, Child, key_encoder)

    class TestKeys:
        """Unit tests for the `keys` method."""

        def test_empty(self, list_dir: FileListDir[Family, int, Child]):
            """It should return an empty sequence."""
            assert_that(tuple(list_dir.keys())).is_empty()

        def test_with_items(self, tmp_path, list_dir: FileListDir[Family, int, Child]):
            """It should return an empty sequence."""
            tmp_path.joinpath("person_123.json").write_text("{bad")
            assert_that(list_dir.keys()).contains(123)
