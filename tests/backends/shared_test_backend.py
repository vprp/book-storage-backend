"""Unit tests for ...

(c) Raise Partner 2022
Vincent Pfister
"""
from abc import ABC, abstractmethod
from pathlib import Path
from typing import Callable

from _pytest.fixtures import fixture
from assertpy import assert_that  # type: ignore[import]
from pytest import fixture

from book_storage_backend.core.backend import StorageBackend
from book_storage_backend.core.model import Book, Bookcase, Library, Shelf


# noinspection PyMethodMayBeStatic
class AbstractTestStorageBackend(ABC):
    """Abstract base class for StorageBackend implementation unit tests."""

    @fixture()
    def library(self) -> Library:
        """Make a test library."""
        return Library()

    @abstractmethod
    @fixture()
    def backend_factory(self, path) -> Callable[[], StorageBackend]:
        """Backend factory, must override in subclass."""
        pass

    def test_create_backend(
        self, backend_factory: Callable[[], StorageBackend]
    ) -> None:
        """It should create the backend."""
        assert_that(backend_factory()).is_not_none()

    def test_clear_empty(self, backend_factory: Callable[[], StorageBackend]):
        """It should do nothing if the library does not exist."""
        backend_factory().clear()

    def test_clear_existing(
        self, library: Library, backend_factory: Callable[[], StorageBackend]
    ):
        """It should do nothing if the library does not exist."""
        backend_factory().get_library()
        backend_factory().clear()


class AbstractTestBackendUsage(ABC):
    """Abstract base for usage tests for backend implementations."""

    @abstractmethod
    @fixture()
    def backend_factory(self) -> Callable[[], StorageBackend]:
        pass

    class TestCreateOneBook:
        """Use case: create one book and the intermediary items."""

        @fixture()
        def book(self, backend_factory: Callable[[], StorageBackend]) -> Book:
            library = backend_factory().get_library()

            bookcase = library.bookcases.add(123, Bookcase())
            shelf = bookcase.shelves.add("abc", Shelf())
            book = shelf.books.add(1, Book(1, "title", "author"))
            return book

        def test_get_book(
            self,
            backend_factory: Callable[[], StorageBackend],
            book: Book,
            tmp_path: Path,
        ) -> None:
            """It should read the book just created."""
            library = backend_factory().get_library()
            bookcase = library.bookcases.get(123)
            shelf = bookcase.shelves.get("abc")
            actual = shelf.books.get(1)
            assert_that(actual).is_equal_to(book)
