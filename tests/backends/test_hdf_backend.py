"""Unit tests for the hdf implementation of the StorageBackend.

(c) Raise Partner 2022
Vincent Pfister
"""
from pathlib import Path
from typing import Callable, Type, TypeVar

import numpy as np
import pandas as pd
from assertpy import assert_that  # type: ignore[import]
from pytest import fixture, mark, param

from book_storage_backend.backends.hdf_backend import (
    BaseHdfObjectBackend,
    HdfBackend,
    HdfBookBackend,
    HdfBookcaseBackend,
    HdfGroupItemsMapper,
    HdfLibraryBackend,
    HdfShelfBackend,
    split_data_path,
)
from book_storage_backend.backends.key_encoder import StrKeyEncoder
from book_storage_backend.core.base_backend import BaseObjectBackend
from book_storage_backend.core.model import Book, Bookcase, Library, Shelf
from tests.backends.backend_fixtures import (
    BookcaseBackendFixtures,
    LibraryBackendFixtures,
    ShelfBackendFixtures,
)
from tests.backends.shared_test_backend import AbstractTestBackendUsage
from tests.core.abstract_test_base_backend import (
    BaseTestItemsBackend,
    BaseTestObjectBackend,
    BaseTestObjectBackendImplementation,
    Person,
)

T = TypeVar("T")


@fixture()
def hdf_file(tmp_path) -> Path:
    return tmp_path.joinpath("data.h5")


class TestBaseHdfObjectBackend(BaseTestObjectBackendImplementation):
    """Unit tests for the BaseHdfObjectBackend class."""

    @fixture()
    def backend_factory(  # type: ignore[override]
        self, hdf_file: Path
    ) -> Callable[[Type[Person]], BaseObjectBackend[Person]]:
        """Make a test flat-file backend for the `klass` parameter."""
        return lambda klass: BaseHdfObjectBackend(hdf_file, klass, "sub")

    class TestWriteObj(BaseTestObjectBackendImplementation.TestWriteObj):
        """Unit tests for the write_obj method specific for hdf."""

        def test_creates_file(
            self,
            backend_with_john: BaseHdfObjectBackend[Person],
            hdf_file: Path,
        ) -> None:
            """It should create the hdf file."""
            assert_that(backend_with_john).is_not_none()
            assert_that(hdf_file.is_file(), f"[{hdf_file}].is_file()").is_true()


class TestSplitDataPath:
    """Unit tests for the split_data_path method."""

    @mark.parametrize(
        "path, head, tail",
        [
            param("test", "/", "test"),
            param("/test", "/", "test"),
            param("dir/test", "/dir", "test"),
            param("/dir/test", "/dir", "test"),
        ],
    )
    def test_split(self, path: str, head: str, tail: str) -> None:
        """It should split the path in (head, tail)."""
        assert_that(split_data_path(path)).is_equal_to((head, tail))


class TestHdfGroupItemMapper:
    """Unit tests for the HdfGroupItemsMapper."""

    @fixture()
    def key_encoder(self) -> StrKeyEncoder:
        return StrKeyEncoder("mykey")

    @fixture()
    def mapper(
        self, hdf_file: Path, key_encoder: StrKeyEncoder
    ) -> HdfGroupItemsMapper[str]:
        return HdfGroupItemsMapper[str](hdf_file, key_encoder)

    class TestKeys:
        """Unit test for the `keys` method."""

        def test_no_file(self, hdf_file: Path, key_encoder: StrKeyEncoder) -> None:
            """It should return empty keys when there is no file."""
            mapper = HdfGroupItemsMapper[str](hdf_file, key_encoder)
            assert_that(tuple(mapper.keys())).is_empty()

        def test_no_subgroup_file(
            self, hdf_file: Path, key_encoder: StrKeyEncoder
        ) -> None:
            """It should return empty keys when the file has no subgroup."""
            with pd.HDFStore(hdf_file, "w") as ds:
                ds["test"] = pd.DataFrame()
            mapper = HdfGroupItemsMapper[str](hdf_file, key_encoder)
            assert_that(tuple(mapper.keys())).is_empty()

        def test_missing_data_path(
            self, hdf_file: Path, key_encoder: StrKeyEncoder
        ) -> None:
            """It should return empty keys when the data path does not exist."""
            with pd.HDFStore(hdf_file, "w") as ds:
                ds["test"] = pd.DataFrame()
            mapper = HdfGroupItemsMapper[str](
                hdf_file, key_encoder, data_path="/invalid/path/"
            )
            assert_that(tuple(mapper.keys())).is_empty()

        def test_with_two_keys(
            self, hdf_file: Path, key_encoder: StrKeyEncoder
        ) -> None:
            """It should return the 2 keys created in the file."""
            with pd.HDFStore(hdf_file, "w") as ds:
                ds["/prefix/mykey_abc/test"] = pd.DataFrame()
                ds["/prefix/mykey_def/test"] = pd.DataFrame()
            mapper = HdfGroupItemsMapper[str](
                hdf_file, key_encoder, data_path="/prefix"
            )
            assert_that(tuple(mapper.keys())).contains_only("abc", "def")

    class TestPathForKey:
        """Unit tests for the `path_for_key` method."""

        @mark.parametrize(
            "prefix, key, expected",
            [
                param("", "abc", "/mykey_abc", id="empty prefix"),
                param("/", "abc", "/mykey_abc", id="prefix=/"),
                param("here", "abc", "/here/mykey_abc", id="prefix=here"),
                param("here/", "abc", "/here/mykey_abc", id="prefix=here/"),
                param("/here", "abc", "/here/mykey_abc", id="prefix=/here"),
                param("/", "", "/mykey_", id="empty key"),
            ],
        )
        def test_with_prefix(
            self,
            hdf_file: Path,
            key_encoder: StrKeyEncoder,
            prefix: str,
            key: str,
            expected: str,
        ):
            """It should return the key path for the given prefix and key."""
            mapper = HdfGroupItemsMapper[str](hdf_file, key_encoder, data_path=prefix)
            assert_that(mapper.path_for_key(key)).is_equal_to(expected)


class TestHdfBookBackend:
    """Unit tests for the HDF implementation of HdfBookBackend."""

    @fixture
    def book(self) -> Book:
        return Book(123, "title", "author")

    @fixture()
    def hdf_file(self, tmp_path: Path) -> Path:
        return tmp_path.joinpath("data.h5")

    @fixture()
    def backend(self, hdf_file) -> HdfBookBackend:
        return HdfBookBackend(hdf_file, "abc/def")

    class TestWriteObj:
        """Unit tests for _write_obj()."""

        def test_create_data_frame(
            self, backend: HdfBookBackend, hdf_file: Path, book: Book
        ):
            backend._write_obj(book)
            with pd.HDFStore(hdf_file, "r") as ds:
                actual = ds["abc/def"]
                assert isinstance(actual, pd.DataFrame)
                pd.testing.assert_frame_equal(actual, pd.DataFrame([book]))

    class TestReadObj:
        """Unit tests for _read_obj()."""

        def test_get_data_frame(
            self, backend: HdfBookBackend, hdf_file: Path, book: Book
        ):
            with pd.HDFStore(hdf_file, "w") as ds:
                ds["abc/def"] = pd.DataFrame([book])
            assert_that(backend._read_obj()).is_equal_to(book)


class TestHdfShelfBackend(
    ShelfBackendFixtures,
    BaseTestObjectBackend[Shelf, HdfShelfBackend, Book, int, HdfBookBackend],
):
    """Unit tests for the HDF implementation of HdfShelfBackend."""

    @fixture()
    def backend_factory(self, hdf_file: Path) -> Callable[[], HdfShelfBackend]:  # type: ignore[override]
        return lambda: HdfShelfBackend(hdf_file, "shelf_abc")

    @fixture()
    def item_backend_factory(self, hdf_file: Path) -> Callable[[], HdfBookBackend]:  # type: ignore[override]
        return lambda: HdfBookBackend(hdf_file, "shelf_abc/book_123")


class TestHdfBookcaseBackend(
    BookcaseBackendFixtures,
    BaseTestItemsBackend[Bookcase, HdfBookcaseBackend, Shelf, str, HdfShelfBackend],
):
    """Unit tests for the HDF implementation of HdfBookcaseBackend."""

    @fixture()
    def backend_factory(self, hdf_file: Path) -> Callable[[], HdfBookcaseBackend]:  # type: ignore[override]
        return lambda: HdfBookcaseBackend(hdf_file)

    @fixture()
    def item_backend_factory(self, hdf_file: Path) -> Callable[[], HdfShelfBackend]:  # type: ignore[override]
        return lambda: HdfShelfBackend(hdf_file, "shelf_abc")


class TestHdfLibraryBackend(
    LibraryBackendFixtures,
    BaseTestItemsBackend[Library, HdfLibraryBackend, Bookcase, int, HdfBookcaseBackend],
):
    """Unit tests for the HDF implementation of LibraryBackend."""

    @fixture()
    def backend_factory(self, hdf_file: Path) -> Callable[[], HdfLibraryBackend]:  # type: ignore[override]
        return lambda: HdfLibraryBackend(hdf_file)

    @fixture()
    def item_backend_factory(self, hdf_file: Path) -> Callable[[], HdfBookcaseBackend]:  # type: ignore[override]
        return lambda: HdfBookcaseBackend(hdf_file)


class TestHdfBackendUsage(AbstractTestBackendUsage):
    """Integrated usage scenarios."""

    @fixture()
    def backend_factory(self, hdf_file: Path) -> Callable[[], HdfBackend]:  # type: ignore[override]
        return lambda: HdfBackend(hdf_file)
