"""Unit tests for DataDir.

(c) Raise Partner 2022
Vincent Pfister
"""
from dataclasses import dataclass
from unittest.mock import patch

import jsonpickle  # type: ignore[import]
from assertpy import assert_that  # type: ignore[import]
from pytest import fixture, mark, param, raises

from book_storage_backend.backends.file_tools import (
    DATA_JSON_FILE,
    DataDir,
    check_data_dir_path,
)
from book_storage_backend.backends.key_encoder import StrKeyEncoder
from book_storage_backend.core.error import StorageError

PREFIX = "person"


@dataclass
class Person:
    """Test class representing a person, used as DataDir target type."""

    name: str


@dataclass
class Room:
    """Test class different from Person."""

    surface: float


class TestDataDir:
    """Unit tests for the DataDir class."""

    @fixture()
    def path(self, tmp_path):
        return tmp_path

    @fixture()
    def bob(self):
        return Person("bob")

    @fixture()
    def alice(self):
        return Person("alice")

    @fixture()
    def path_with_bob(self, path, bob):
        path.joinpath(DATA_JSON_FILE).write_text(jsonpickle.encode(bob))
        return path

    @fixture()
    def klass(self):
        return Person

    @fixture()
    def encoder(self):
        return StrKeyEncoder(PREFIX)

    @fixture()
    def data_dir(self, path, klass, encoder) -> DataDir["Person", str]:
        return DataDir(path, klass, encoder)

    @fixture()
    def data_dir_with_bob(
        self, path_with_bob, klass, encoder
    ) -> DataDir["Person", str]:
        return DataDir(path_with_bob, klass, encoder)

    @patch("book_storage_backend.backends.file_tools.check_data_dir_path")
    def test_init_calls_check_path(self, mock_check_path, path, klass, encoder):
        """It should call the check_data_dir_path method in __init__."""
        mock_check_path.return_value = path
        _ = DataDir(path, klass, encoder)
        mock_check_path.assert_called_once_with(path)

    class TestCheckPath:
        """Unit tests for the check_data_dir method."""

        def test_returns_path(self, path):
            """It should return the given path."""
            assert_that(check_data_dir_path(path)).is_equal_to(path)

        def test_raise_for_non_existing(self, path):
            """It should raise OSError if the path is not a directory."""
            new_path = path.joinpath("does_not_exist")
            with raises(FileNotFoundError):
                check_data_dir_path(new_path)

        def test_raise_for_non_dir(self, path):
            """It should raise OSError if the path is not a directory."""
            new_path = path.joinpath("out.txt")
            new_path.write_text("blabla")
            with raises(NotADirectoryError):
                check_data_dir_path(new_path)

        def test_raise_for_non_writable_dir(self, path):
            """It should raise OSError if the path is not a directory."""
            new_path = path.joinpath("sub")
            new_path.mkdir(mode=0o400)
            with raises(PermissionError):
                check_data_dir_path(new_path)

    class TestWriteData:
        """Unit tests for the write_data method."""

        def test_create_data_file(self, data_dir, bob, path):
            """It should create the object data file."""
            data_dir.write_data(bob)
            file = path.joinpath(DATA_JSON_FILE)
            assert_that(file.exists(), "path exists").is_true()
            assert_that(file.is_file(), "path is file").is_true()

        def test_does_not_overwrite_data(self, data_dir_with_bob, alice):
            """It should raise if overwriting data file."""
            with raises(StorageError):
                data_dir_with_bob.write_data(alice)

        def test_overwrites_data_if_allowed(self, data_dir_with_bob, alice):
            """It should overwrite the data file if allowed."""
            data_dir_with_bob.write_data(alice, overwrite=True)
            assert_that(data_dir_with_bob.data_file.read_text()).contains(alice.name)

        def test_none_raises(self, data_dir):
            """It should raise ValueError if object is None."""
            with raises(TypeError):
                data_dir.write_data(None)

    class TestReadData:
        """Unit tests for the `read_data` method."""

        def test_object_is_read(self, data_dir_with_bob, bob):
            """It should read the data."""
            assert_that(data_dir_with_bob.read_data()).is_equal_to(bob)

        def test_raises_if_missing(self, data_dir):
            """It should raise StorageError if data_file is missing."""
            with raises(StorageError):
                data_dir.read_data()

        def test_raises_if_not_a_file(self, data_dir):
            """It should raise StorageError if data_file is not a file."""
            data_dir.data_file.mkdir()
            with raises(StorageError):
                data_dir.read_data()

        @fixture()
        def data_dir_with_bob_unreadable(self, data_dir_with_bob):
            """Locally make datafile unreadable.

            must be done this way to allow garbage collection."""
            data_dir_with_bob.data_file.chmod(0o220)
            yield data_dir_with_bob
            data_dir_with_bob.data_file.chmod(0o755)

        def test_raises_if_unreadable(self, data_dir_with_bob_unreadable):
            """It should raise StorageError if data_file is unreadable."""
            with raises(StorageError):
                data_dir_with_bob_unreadable.read_data()

        def test_raises_if_invalid_json(self, data_dir_with_bob):
            """It should raise StorageError if data_file is not a json file."""
            data_dir_with_bob.data_file.write_text("not)good!stuff")
            with raises(StorageError):
                data_dir_with_bob.read_data()

        def test_raises_if_file_is_empty(self, data_dir_with_bob):
            """It should raise StorageError if data_file is empty."""
            data_dir_with_bob.data_file.write_text("")
            with raises(StorageError):
                data_dir_with_bob.read_data()

        def test_raises_if_invalid_type(self, data_dir_with_bob):
            """It should raise StorageError if object in the data_file is not Person."""
            data_dir_with_bob.data_file.write_text(jsonpickle.encode(Room(10)))
            with raises(TypeError):
                data_dir_with_bob.read_data()

    @mark.parametrize(
        "files, dirs, expected",
        [
            param(
                [],
                [f"{PREFIX}_abc", f"{PREFIX}_def"],
                ["abc", "def"],
                id="only keys",
            ),
            param(
                [f"{PREFIX}_file_not_dir", "other_file"],
                [f"{PREFIX}_abc"],
                ["abc"],
                id="1 key and files",
            ),
            param(
                [],
                [f"{PREFIX}_abc", "other_dir"],
                ["abc"],
                id="1 key and dirs",
            ),
            param(
                [f"{PREFIX}_file_not_dir", "other_file"],
                [f"{PREFIX}abc", "other_dir"],
                [],
                id="0 key with files and dirs",
            ),
            param(
                [],
                [f"{PREFIX}_"],
                [""],
                id="1 key with empty name",
            ),
        ],
    )
    def test_read_keys(self, data_dir, path, files, dirs, expected):
        """It should read the keys as set by `files` and `dirs`."""
        [path.joinpath(name).touch() for name in files]
        [path.joinpath(name).mkdir() for name in dirs]
        assert_that(tuple(sorted(data_dir.keys()))).is_equal_to(tuple(expected))

    def test_path_for_key(self, data_dir, path):
        """It should return the correct key path."""
        actual = data_dir.path_for_key("xyz")
        assert_that(actual).is_equal_to(path.joinpath(f"{PREFIX}_xyz"))
