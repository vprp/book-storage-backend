"""Module.

(c) Raise Partner 2022
Vincent Pfister
"""
from dataclasses import dataclass
from pathlib import Path
from typing import Callable, Type, TypeVar
from unittest.mock import patch

from assertpy import assert_that  # type: ignore[import]
from pytest import fixture, mark, param, raises

from book_storage_backend.backends.base_flat_file_backend import (
    BaseFlatFileObjectBackend,
)
from book_storage_backend.backends.file_tools import safe_create_dir
from book_storage_backend.backends.flat_file_backend import (
    LIBRARY_DATA_DIR,
    FlatFileBackend,
    FlatFileBookBackend,
    FlatFileBookcaseBackend,
    FlatFileLibraryBackend,
    FlatFileShelfBackend,
)
from book_storage_backend.core.backend import StorageBackend
from book_storage_backend.core.error import StorageError
from book_storage_backend.core.model import Book, Bookcase, Library, Shelf
from tests.backends.backend_fixtures import (
    BookcaseBackendFixtures,
    LibraryBackendFixtures,
    ShelfBackendFixtures,
)
from tests.backends.shared_test_backend import (
    AbstractTestBackendUsage,
    AbstractTestStorageBackend,
)
from tests.core.abstract_test_base_backend import (
    BaseTestItemsBackend,
    BaseTestObjectBackend,
    BaseTestObjectBackendImplementation,
    Person,
)

T = TypeVar("T")


class TestBaseFlatFileObjectBackend(BaseTestObjectBackendImplementation):
    """Unit tests for the BaseFlatFileObjectBackend class."""

    @fixture()
    def data_file(self, tmp_path) -> Path:
        """Make the data fle path."""
        return tmp_path.joinpath("sub_dir", "data.json")

    @fixture()
    def backend_factory(  # type: ignore[override]
        self, data_file
    ) -> Callable[[Type[T]], BaseFlatFileObjectBackend[T]]:
        """Make a test flat-file backend for the `klass` parameter."""
        return lambda klass: BaseFlatFileObjectBackend(data_file, klass)

    class TestWriteObj(BaseTestObjectBackendImplementation.TestWriteObj):
        """Unit tests for the write_obj method specific for flat-file."""

        def test_creates_file(
            self,
            backend_with_john: BaseFlatFileObjectBackend[Person],
            data_file: Path,
        ) -> None:
            """It should create the data file."""
            assert_that(backend_with_john).is_not_none()
            assert_that(data_file.is_file(), "data_file.is_file()").is_true()

    class TestDelObj(BaseTestObjectBackendImplementation.TestDelObj):
        """Unit tests for the _del_obj method specific for flat-file."""

        def test_removes_file(
            self, backend_with_john: BaseFlatFileObjectBackend[Person], data_file: Path
        ):
            """It should remove the data file when deleting the object."""
            backend_with_john._del_obj()
            assert_that(data_file.is_file(), "data_file.is_file()").is_false()


@fixture()
def base_dir(tmp_path):
    return tmp_path.joinpath("base_dir")


class TestFlatFileShelfBackend(
    ShelfBackendFixtures,
    BaseTestObjectBackend[Shelf, FlatFileShelfBackend, Book, int, FlatFileBookBackend],
):
    """Unit tests for flat-file implementation of ShelfBackend."""

    @fixture()
    def backend_factory(self, base_dir) -> Callable[[], FlatFileShelfBackend]:  # type: ignore[override]
        return lambda: FlatFileShelfBackend(base_dir)

    @fixture()
    def item_backend_factory(self, base_dir) -> Callable[[], FlatFileBookBackend]:  # type: ignore[override]
        return lambda: FlatFileBookBackend(base_dir)


class TestFlatFileBookcaseBackend(
    BookcaseBackendFixtures,
    BaseTestItemsBackend[
        Bookcase, FlatFileBookcaseBackend, Shelf, str, FlatFileShelfBackend
    ],
):
    """Unit tests for flat-file implementation of ShelfBackend."""

    @fixture()
    def backend_factory(self, base_dir: Path) -> Callable[[], FlatFileBookcaseBackend]:  # type: ignore[override]
        return lambda: FlatFileBookcaseBackend(base_dir)

    @fixture()
    def item_backend_factory(  # type: ignore[override]
        self, base_dir: Path
    ) -> Callable[[], FlatFileShelfBackend]:
        return lambda: FlatFileShelfBackend(base_dir)


class TestFlatFileLibraryBackend(
    LibraryBackendFixtures,
    BaseTestItemsBackend[
        Library, FlatFileLibraryBackend, Bookcase, int, FlatFileBookcaseBackend
    ],
):
    """Unit tests for flat-file implementation of ShelfBackend."""

    @fixture()
    def backend_factory(self, base_dir: Path) -> Callable[[], FlatFileLibraryBackend]:  # type: ignore[override]
        return lambda: FlatFileLibraryBackend(base_dir)

    @fixture()
    def item_backend_factory(  # type: ignore[override]
        self, base_dir: Path
    ) -> Callable[[], FlatFileBookcaseBackend]:
        return lambda: FlatFileBookcaseBackend(base_dir)


class TestFlatFileStorageBackend(AbstractTestStorageBackend):
    """Unit tests for the flat-file implementation of StorageBackend."""

    @fixture()
    def backend_factory(self, base_dir: Path) -> Callable[[], StorageBackend]:
        def factory() -> StorageBackend:
            return FlatFileBackend(base_dir)

        return factory


class TestSafeCreateDir:
    """Unit tests for the safe_create_dir method."""

    @fixture()
    def message(self) -> str:
        return "my stuff"

    def test_create(self, tmp_path: Path, message: str) -> None:
        """It should create the directory when it does not exist."""
        path = tmp_path.joinpath("sub")
        safe_create_dir(path, message)
        assert_that(path.is_dir(), "path.is_dir()").is_true()

    def test_create_exists_raises(self, tmp_path: Path, message: str) -> None:
        """It should raise KeyError if the directory exists."""
        path = tmp_path.joinpath("sub")
        path.mkdir()
        with raises(KeyError) as err:
            safe_create_dir(path, message)
        assert_that(str(err.value)).contains(message)

    @patch("os.makedirs", side_effect=OSError())
    def test_create_os_error_raises(self, tmp_path: Path, message: str) -> None:
        """It should raise StorageError if creating the directory raises OSError."""
        path = tmp_path.joinpath("sub")
        path.mkdir()
        with raises(StorageError) as err:
            safe_create_dir(path, message)
        assert_that(str(err.value)).contains(message)


class TestFlatFileBackendUsage(AbstractTestBackendUsage):
    """Integrated usage scenarios."""

    @fixture()
    def backend_factory(self, tmp_path: Path) -> Callable[[], FlatFileBackend]:  # type: ignore[override]
        return lambda: FlatFileBackend(tmp_path)

    class TestCreateOneBook(AbstractTestBackendUsage.TestCreateOneBook):
        """Use case: create one book and the intermediary items."""

        @mark.parametrize(
            "name",
            [
                param("data.json", id="library data"),
                param("bookcase_123/data.json", id="bookcase data"),
                param("bookcase_123/shelf_abc/data.json", id="shelf data"),
                param("bookcase_123/shelf_abc/book_001.json", id="book data"),
            ],
        )
        def test_has_data_file(self, book: Book, tmp_path, name: str) -> None:
            """It should create the data files."""
            path = tmp_path.joinpath(LIBRARY_DATA_DIR, name)
            assert_that(path.exists(), f"<{path}>.exists()").is_true()
