"""Unit tests for in-memory backends.

(c) Raise Partner 2022
Vincent Pfister
"""
from typing import Callable

from pytest import fixture

from book_storage_backend.backends.in_memory_backend import (
    InMemoryBackend,
    InMemoryBookBackend,
    InMemoryBookcaseBackend,
    InMemoryLibraryBackend,
    InMemoryShelfBackend,
)
from book_storage_backend.core.backend import StorageBackend
from book_storage_backend.core.model import Book, Bookcase, Library, Shelf
from tests.backends.backend_fixtures import (
    BookcaseBackendFixtures,
    LibraryBackendFixtures,
    ShelfBackendFixtures,
)
from tests.backends.shared_test_backend import AbstractTestStorageBackend
from tests.core.abstract_test_base_backend import (
    BaseTestItemsBackend,
    BaseTestObjectBackend,
)


class TestInMemoryShelfBackend(
    ShelfBackendFixtures,
    BaseTestObjectBackend[Shelf, InMemoryShelfBackend, Book, int, InMemoryBookBackend],
):
    """Unit tests for the in-memory implementation of ShelfBackend."""

    @fixture()
    def backend_factory(self) -> Callable[[], InMemoryShelfBackend]:
        return InMemoryShelfBackend

    @fixture()
    def item_backend_factory(self) -> Callable[[], InMemoryBookBackend]:
        return InMemoryBookBackend


class TestInMemoryBookcaseBackend(
    BookcaseBackendFixtures,
    BaseTestItemsBackend[
        Bookcase, InMemoryBookcaseBackend, Shelf, str, InMemoryShelfBackend
    ],
):
    """Unit tests for the in-memory implementation of BookcaseBackend."""

    @fixture()
    def backend_factory(self) -> Callable[[], InMemoryBookcaseBackend]:
        return InMemoryBookcaseBackend

    @fixture()
    def item_backend_factory(self) -> Callable[[], InMemoryShelfBackend]:
        return InMemoryShelfBackend


class TestInMemoryLibraryBackend(
    LibraryBackendFixtures,
    BaseTestItemsBackend[
        Library, InMemoryLibraryBackend, Bookcase, int, InMemoryBookcaseBackend
    ],
):
    """Unit tests for the in-memory implementation of LibraryBackend."""

    @fixture()
    def backend_factory(self) -> Callable[[], InMemoryLibraryBackend]:
        return InMemoryLibraryBackend

    @fixture()
    def item_backend_factory(self) -> Callable[[], InMemoryBookcaseBackend]:
        return InMemoryBookcaseBackend


class TestInMemoryStorageBackend(AbstractTestStorageBackend):
    """Unit tests for the in-memory implementation of StorageBackend."""

    @fixture()
    def backend_factory(self) -> Callable[[], StorageBackend]:  # type: ignore[override]
        def factory() -> StorageBackend:
            return InMemoryBackend()

        return factory
