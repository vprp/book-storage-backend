"""Unit tests for KeyEncoder classes.

(c) Raise Partner 2022
Vincent Pfister
"""
from typing import Dict

from assertpy import assert_that  # type: ignore[import]
from pytest import mark, param

from book_storage_backend.backends.key_encoder import StrKeyEncoder


class TestKeyEncoder:
    """Unit tests for the KeyEncoder class."""

    @mark.parametrize(
        "prefix, suffix, expected",
        [
            param("pre", "", {"": "pre_", "abc": "pre_abc"}, id="no suffix"),
            param(
                "pre",
                "post",
                {"": "pre_.post", "abc": "pre_abc.post"},
                id="with suffix",
            ),
        ],
    )
    def test_encodes(self, prefix: str, suffix: str, expected: Dict[str, str]) -> None:
        encoder = StrKeyEncoder(prefix, suffix)
        for value, result in expected.items():
            assert_that(encoder.encode(value)).is_equal_to(result)
