"""Module.

(c) Raise Partner 2022
Vincent Pfister
"""
from _pytest.fixtures import fixture

from book_storage_backend.core.model import Book, Bookcase, Shelf


class ShelfBackendFixtures:
    """Fixtures for tests of the ShelfBackend implementations.

    (items/keys used by the unit tests)"""

    @fixture()
    def item_key(self) -> int:
        return 123

    @fixture()
    def new_item_key(self) -> int:
        return 456

    @fixture()
    def item(self) -> Book:
        return Book(11, "title 11", "author 11")

    @fixture()
    def new_item(self) -> Book:
        return Book(22, "title 22", "author 22")


class BookcaseBackendFixtures:
    """Fixtures for tests of the BookcaseBackend implementations.

    (items/keys used by the unit tests)"""

    @fixture()
    def item_key(self) -> str:
        return "abc"

    @fixture()
    def new_item_key(self) -> str:
        return "def"

    @fixture()
    def item(self) -> Shelf:
        return Shelf(width=11)

    @fixture()
    def new_item(self) -> Shelf:
        return Shelf(width=22)


class LibraryBackendFixtures:
    """Fixtures for tests of the LibraryBackend implementations.

    (items/keys used by the unit tests)"""

    @fixture()
    def item_key(self) -> int:
        return 11

    @fixture()
    def new_item_key(self) -> int:
        return 99

    @fixture()
    def item(self) -> Bookcase:
        return Bookcase(height=111)

    @fixture()
    def new_item(self) -> Bookcase:
        return Bookcase(height=999)
