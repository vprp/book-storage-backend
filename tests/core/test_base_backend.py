"""Unit tests for the base backend classes.


This test modules derives concrete test classes from thos in `abstract_test_base_backend`.
The use the following object/backend hierarchy:

```
object X: ----------------> BackendX <- BX
  - items: [KY] -> Y

object Y: ----------------> BackendY <- BY
  - items: [KZ] -> Z

object Z: ----------------> BackendY <- BZ
```

X, Y and Z are ad-hoc *dataclasses*.
KY and KZ are identified str types used as keys for Y and Z items.
BX, BY and BZ are BaseItemsBackend or BaseObjectBackend (for BZ) with the correct generic binding.
BackendX, BackendY and BackendZ are mocks making BX, BY and BZ concrete with Mocks for abstract methods.

The actual test classes implement the abstract fixtures.

(c) Raise Partner 2022
Vincent Pfister
"""
# flake8: noqa

from dataclasses import dataclass
from typing import Callable, Generator

from assertpy import assert_that  # type: ignore[import]
from pytest import fixture, mark, param, raises
from typing_extensions import NewType

from book_storage_backend.core.base_backend import BaseItemsBackend, BaseObjectBackend
from book_storage_backend.core.storage import Storage, WithStorage
from tests.core.abstract_test_base_backend import (
    BaseTestItemsBackend,
    BaseTestObjectBackend,
)
from tests.testing_utils import mock_abstract_generic, reset_inner_mocks

KY = NewType("KY", str)
KZ = NewType("KZ", str)


@dataclass
class Z:
    name: str = "this is a Z"


@dataclass
class Y(WithStorage[KZ, Z]):
    name: str = "i'm a Z"

    @property
    def items(self) -> Storage[KZ, Z]:
        return self._storage


@dataclass
class X(WithStorage[KY, Y]):
    name: str = "the best X"

    @property
    def items(self) -> Storage[KY, Y]:
        return self._storage


BX = BaseItemsBackend[X, KY, Y]
BY = BaseItemsBackend[Y, KZ, Z]
BZ = BaseObjectBackend[Z]


BackendX = mock_abstract_generic(BX, "BX")
BackendY = mock_abstract_generic(BY, "BY")
BackendZ = mock_abstract_generic(BZ, "BZ")


class TestItemsBackend(BaseTestItemsBackend[X, BX, Y, KY, BY]):
    """Unit tests for the BaseItemsBackend: BX has BY as items backend."""

    @fixture(scope="function")
    def backend_factory(self) -> Callable[[], BX]:
        """Factory returns a backend for X."""
        return BackendX

    @fixture(scope="function")
    def item_backend_factory(self) -> Callable[[], BY]:
        """Factory returns an item backend for Y."""
        return BackendY

    @fixture()
    def item_key(self) -> KY:
        """make key for an item."""
        return KY("item_key")

    @fixture()
    def new_item_key(self) -> KY:
        """make key for a new item."""
        return KY("new_item_key")

    @fixture()
    def item(self) -> Y:
        """make an item."""
        return Y("item")

    @fixture()
    def new_item(self) -> Y:
        """make a new item."""
        return Y("new_item")


class TestObjectBackend(BaseTestObjectBackend[Y, BY, Z, KZ, BZ]):
    """Unit tests for the BaseObjectBackend: BY has BZ as objects backend."""

    @fixture(scope="function")
    def backend_factory(self) -> Callable[[], BY]:
        """Factory returns a backend for Y."""
        return BackendY

    @fixture(scope="function")
    def item_backend_factory(self) -> Callable[[], BZ]:
        """Factory returns an item backend for Z."""
        return BackendZ

    @fixture()
    def item_key(self) -> KZ:
        """make key for an item."""
        return KZ("item_key")

    @fixture()
    def new_item_key(self) -> KZ:
        """make key for a new item."""
        return KZ("new_item_key")

    @fixture()
    def item(self) -> Z:
        """make an item."""
        return Z("item")

    @fixture()
    def new_item(self) -> Z:
        """make a new item."""
        return Z("new_item")
