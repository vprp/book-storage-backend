"""Provides abstract generic base classes for testing backends.

The generic _TypeVar_ used are:
- `TI`: Type of the objects using the backend
- `B: Type of the backend
- `TI`: Type of the items used in the object's storage
- `KI`: Type of the keys of the items
- `BI`: Type of the items' backend

This module provides 2 generic abstract base classes:
- `BaseTestObjectBackend[TI, B, TI, KI, BI]` for backends whose item's backend
  is an object backend
- `BaseTestItemsBackend[TI, B, TI, KI, BI]` for backends whose item's backend
  is am Items backend

It also includes the `BaseTestObjectBackendImplementation` abstract base test class
for making test classes for the concrete implementations of the BaseObjectBackend
itself.

See the `base_backend` module for details on object and items backends.

In order to test a specific backend, derive a test class from one of the 2 base classes,
and implement the abstract fixtures:
- `backend_factory` returns a factory function for creating backends
- `item_backend_factory` returns a factory function for creating backends for items
- `item_key` is the default key inserted in *backend_with_item*
- `item` is the default item inserted in *backend_with_item*
- `new_item_key` is another item key
- `new_item` is another item

The main fixtures are
- `backend`: produces a clean empty backend
- `item_backend`: produces a clean backend for items, and wires it to the main backend
- `backend_with_item` a backend with the `item` and `item_key` inserted

In addition, in order to test the base object backend implementation, derive a test
class from `BaseTestObjectBackendImplementation`, and provide a `backend_factory`
fixture.

(c) Raise Partner 2022
Vincent Pfister
"""
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Callable, Generator, Generic, Type, TypeVar
from unittest.mock import patch

from assertpy import assert_that  # type: ignore[import]
from pytest import fixture, mark, raises

from book_storage_backend.core.base_backend import BaseItemsBackend, BaseObjectBackend
from book_storage_backend.core.error import StorageError
from tests.testing_utils import reset_inner_mocks

T = TypeVar("T")
B = TypeVar("B", bound=BaseItemsBackend)
TI = TypeVar("TI")
KI = TypeVar("KI")
BI = TypeVar("BI", bound=BaseObjectBackend)


class BaseTestObjectBackend(Generic[T, B, TI, KI, BI], ABC):
    """Generic base for unit tests of backend implementing BaseObjectBackend."""

    @abstractmethod
    @fixture(scope="function")
    def backend_factory(self) -> Callable[[], B]:
        """Factory creating a new, empty test backend."""

    @abstractmethod
    @fixture(scope="function")
    def item_backend_factory(self) -> Callable[[], BI]:
        """Factory creating a new, empty test backend."""

    @fixture(scope="function")
    def backend(self, backend_factory: Callable[[], B]) -> Generator[B, None, None]:
        """Make a test Backend"""
        backend = backend_factory()
        yield backend
        reset_inner_mocks(backend)

    @fixture(scope="function")
    def item_backend(
        self, backend: B, item_backend_factory: Callable[[], BI]
    ) -> Generator[BI, None, None]:
        """Make a test backend whose factory makes an item backend."""
        item_backend = item_backend_factory()
        with patch.object(backend, "_backend_factory", return_value=item_backend):
            reset_inner_mocks(item_backend)
            yield item_backend
            reset_inner_mocks(item_backend)

    @abstractmethod
    @fixture()
    def item_key(self) -> KI:
        """make key for an item."""
        pass

    @abstractmethod
    @fixture()
    def new_item_key(self) -> KI:
        """make key for a new item."""
        pass

    @abstractmethod
    @fixture()
    def item(self) -> TI:
        """make an item."""
        pass

    @abstractmethod
    @fixture()
    def new_item(self) -> TI:
        """make a new item."""
        pass

    @fixture(scope="function")
    def backend_with_item(
        self, backend: B, item_backend: BI, item_key: KI, item: TI
    ) -> Generator[B, None, None]:
        """It should return the item for the existing key."""
        with patch.object(backend, "keys", return_value=(k for k in (item_key,))):
            with patch.object(item_backend, "_read_obj", return_value=item):
                reset_inner_mocks(backend)
                reset_inner_mocks(item_backend)
                yield backend
                reset_inner_mocks(backend)
                reset_inner_mocks(item_backend)

    class TestAdd:
        """Unit tests for the `add` method."""

        def test_calls_backend_factory(
            self, backend: B, item_key: KI, item: TI
        ) -> None:
            """It should call the factory."""
            with patch.object(backend, "_backend_factory") as mock_backend_factory:
                backend.add(item_key, item)
            mock_backend_factory.assert_called_once_with(item_key)

        def test_calls_write_obj(
            self, backend: B, item_backend: BI, item_key: KI, item: TI
        ) -> None:
            """It should write the item's object."""
            with patch.object(item_backend, "_write_obj") as mock_write_obj:
                backend.add(item_key, item)
            mock_write_obj.assert_called_once_with(item)

        def test_existing_key_raises(
            self, backend_with_item: B, item: TI, item_key: KI
        ):
            """It should raise KeyError if adding an existing key."""
            with raises(KeyError):
                backend_with_item.add(item_key, item)

    class TestHasKey:
        """Unit tests for the `_has_key` method."""

        def test_existing_key(self, backend_with_item: B, item_key: KI) -> None:
            """It should return True for an existing key."""
            assert_that(backend_with_item._has_key(item_key)).is_true()

        def test_missing_key(
            self, backend_with_item: B, item: TI, new_item_key: KI
        ) -> None:
            """It should return True for an existing key."""
            assert_that(backend_with_item._has_key(new_item_key)).is_false()

    class TestGet:
        """Unit tests for the `get` method."""

        def test_returns_item(
            self, backend_with_item: B, item_key: KI, item: TI
        ) -> None:
            """It should return the item for the existing key."""
            assert_that(backend_with_item.get(item_key)).is_equal_to(item)

        def test_not_found_raises(self, backend: B, item_key: KI) -> None:
            """It should raise KeyError if the key is not found."""
            with raises(KeyError) as err:
                _ = backend.get(item_key)
            assert_that(str(err.value)).contains(str(item_key))

    class TestSet:
        """Unit tests for the `set` method."""

        def test_writes_object(
            self, backend_with_item: B, item_backend: BI, item_key: KI, new_item: TI
        ) -> None:
            """It should call the _write_obj method."""
            with patch.object(item_backend, "_write_obj") as mock_write_obj:
                backend_with_item.set(item_key, new_item)
            mock_write_obj.assert_called_once_with(new_item)

        def test_not_found_raises(
            self, backend: B, item_backend: BI, item_key: KI, new_item: TI
        ) -> None:
            """It should raise KeyError when calling `set` with a non-existing key."""
            with raises(KeyError) as err:
                backend.set(item_key, new_item)
            assert_that(str(err.value)).contains(str(item_key))

    class TestDelete:
        """Unit tests for the `delete` method."""

        def test_calls_del_obj(
            self, backend_with_item: B, item_backend: BI, item_key: KI
        ) -> None:
            """It should call the _del_obj method."""
            with patch.object(item_backend, "_del_obj") as mock_del_obj:
                backend_with_item.delete(item_key)
                mock_del_obj.assert_called_once()

        def test_raises_not_found(
            self, backend: B, item_backend: BI, item_key: KI
        ) -> None:
            """It should raise KeyError if the key is not found"""
            with raises(KeyError) as err:
                backend.delete(item_key)
            assert_that(str(err.value)).contains(str(item_key))

    class TestAddAndGet:
        """Test chaining add and get."""

        def get_added_item(self, backend: B, item_key: KI, item: TI):
            """It should get the added item."""
            backend.add(item_key, item)
            assert_that(backend.get(item_key)).is_equal_to(item)


class BaseTestItemsBackend(
    BaseTestObjectBackend[T, B, TI, KI, BI], Generic[T, B, TI, KI, BI], ABC
):
    """Generic base for unit tests of backend implementing BaseItemsBackend."""

    class TestAdd(BaseTestObjectBackend.TestAdd):
        """Unit tests for the `add` method specific to Items backends."""

        # @mark.skip()
        def test_unsupported_item_raises(
            self, backend: B, item_backend: BI, item_key: KI
        ):
            """It should raise TypeError if the added item is not a `WithStorage` item.

            This behaviour could be released at some point: it is here only to make sure that
            the call to `use_storage` is not skipped involuntarily by passing the wrong type.
            The other option could be to use _duck typing_ and silently ignore the call to
            `use_storage` if the method doesn't exist on the item. (more risky for now!)

            passing `item_backend` is required to wire the `_backend_factory` method of `backend`.
            """

            @dataclass
            class PlainObject:
                name: str = "this is a PlainObject"

            with raises(TypeError):
                backend.add(item_key, PlainObject("123"))  # type: ignore[arg-type]

        def test_registers_returned_value(
            self, backend: B, item_backend: BI, item_key: KI, item: TI
        ) -> None:
            """It should return the value with the proper backend registered."""
            value = backend.add(item_key, item)
            assert_that(value._storage).is_equal_to(item_backend)

    class TestGet(BaseTestObjectBackend.TestGet):
        """Unit tests for the `get` method specific to Items backends."""

        def test_registers_returned_value(
            self, backend_with_item: B, item_backend: BI, item_key: KI
        ) -> None:
            """It should return the item with the proper backend registered."""
            value = backend_with_item.get(item_key)
            assert_that(value._storage).is_equal_to(item_backend)

    class TestSet(BaseTestObjectBackend.TestSet):
        """Unit tests for the `set` method specific to Items backends."""

        def test_registers_returned_value(
            self, backend_with_item: B, item_backend: BI, item_key: KI, new_item: TI
        ) -> None:
            """It should register the returned value with the item backend."""
            value = backend_with_item.set(item_key, new_item)
            assert_that(value._storage).is_equal_to(item_backend)


@dataclass
class Person:
    name: str


@dataclass
class Animal:
    legs: int


class BaseTestObjectBackendImplementation(ABC):
    """Generic base class for tests of the ObjectBackend implementations.

    Tests here are conducted with fake objects `Person` or `Animal`.
    Extend this class, implement the `backend_factory`, and add tests
    specific to the backend implementation under test (for example:
    verify the presence absence of a file, or its validity, check the
    impact of raising OSError at various points, etc.).
    """

    @abstractmethod
    @fixture()
    def backend_factory(self) -> Callable[[Type[Person]], BaseObjectBackend[Person]]:
        """Factory making a test backend."""
        pass

    @fixture()
    def backend(
        self, backend_factory: Callable[[Type[Person]], BaseObjectBackend[Person]]
    ) -> BaseObjectBackend[Person]:
        """Make a test backend."""
        return backend_factory(Person)

    @fixture()
    def john(self) -> Person:
        """Make a person named John."""
        return Person("John")

    @fixture()
    def backend_with_john(
        self, backend: BaseObjectBackend[Person], john: Person
    ) -> BaseObjectBackend[Person]:
        """Make a test backend with data saved for John."""
        backend._write_obj(john)
        return backend

    class TestWriteObj:
        """Unit tests for the write_obj method."""

        def test_bad_type(self, backend: BaseObjectBackend[Person]) -> None:
            """It should raise type error if value is not a Person."""
            with raises(TypeError):
                backend._write_obj("blabla")  # type: ignore[arg-type]

    class TestReadObj:
        """Unit tests for the _read_obj method."""

        def test_read_after_write(
            self, backend_with_john: BaseObjectBackend[Person], john: Person
        ) -> None:
            """It should read the object just written."""
            assert_that(backend_with_john._read_obj()).is_equal_to(john)

        def test_wrong_type(
            self,
            backend_with_john: BaseObjectBackend[Person],
            backend_factory: Callable[[Type[Person]], BaseObjectBackend[Person]],
        ) -> None:
            """It should raise TypeError if the object read is of the wrong type."""
            assert_that(backend_with_john).is_not_none()
            with raises(TypeError):
                backend_factory(Animal)._read_obj()  # type: ignore[arg-type]

    class TestDelObj:
        """Unit tests for the _del_obj method."""

        def test_removes_makes_read_fail(
            self, backend_with_john: BaseObjectBackend[Person]
        ):
            """It should raise when _read_obj is called after _del_obj."""
            backend_with_john._del_obj()
            with raises(StorageError):
                backend_with_john._read_obj()

        def test_missing_obj_raises(self, backend: BaseObjectBackend[Person]):
            """It should raise OSError when _del_obj is called without a data file."""
            with raises(StorageError):
                backend._del_obj()
