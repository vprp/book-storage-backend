"""
Unit tests for split_key.py
"""
from assertpy import assert_that  # type: ignore[import]
from pytest import raises

from book_storage_backend.utils.split_key import hash_key, split_hash


class TestHashKey:
    """Unit tests for hash_key()"""

    def test_empty_key(self) -> None:
        h = hash_key([])
        assert_that(h).is_equal_to("da39a3ee5e6b4b0d3255bfef95601890afd80709")

    def test_simple_str_key(self) -> None:
        h = hash_key(["abc"])
        assert_that(h).is_equal_to("a9993e364706816aba3e25717850c26c9cd0d89d")

    def test_simple_int_key(self) -> None:
        h = hash_key([123])
        assert_that(h).is_equal_to("40bd001563085fc35165329ea1ff5c5ecbdbbeef")

    def test_multiple_keys(self) -> None:
        h = hash_key(["abc", "def"])
        assert_that(h).is_equal_to("1f8ac10f23c5b5bc1167bda84b833e5c057a77d2")

    def test_multiple_different_keys(self) -> None:
        h = hash_key(["abc", 123])
        assert_that(h).is_equal_to("6367c48dd193d56ea7b0baad25b19455e529f5ee")

    def test_multiple_different_keys_inverted(self) -> None:
        h = hash_key([123, "abc"])
        assert_that(h).is_equal_to("4be30d9814c6d4e9800e0d2ea9ec9fb00efa887b")


class TestSplitHash:
    """Unit tests for split_hash()"""

    _h: str = "abcdef1234567890"

    def test_empty_hash(self) -> None:
        s = split_hash("", mode=[])
        assert_that(s).is_equal_to([])

    def test_zero_mode(self) -> None:
        with raises(ValueError):
            split_hash(self._h, mode=[0])

    def test_negative_mode(self) -> None:
        with raises(ValueError):
            split_hash(self._h, mode=[-1])

    def test_without_mode(self) -> None:
        s = split_hash(self._h, mode=[])
        assert_that(s).is_equal_to(["_abcdef1234567890"])

    def test_with_simple_mode(self) -> None:
        s = split_hash(self._h, mode=[1])
        assert_that(s).is_equal_to(["_a", "_abcdef1234567890"])

    def test_with_multiple_modes(self) -> None:
        s = split_hash(self._h, mode=[2, 2])
        assert_that(s).is_equal_to(["_ab", "_abcd", "_abcdef1234567890"])

    def test_with_long_modes(self) -> None:
        s = split_hash(self._h, mode=[20, 20])
        assert_that(s).is_equal_to(
            ["_abcdef1234567890", "_abcdef1234567890", "_abcdef1234567890"]
        )
