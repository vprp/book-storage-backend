"""
Utils to:
- create hash from an array of keys
- split a hash into several key parts
"""
import hashlib
from typing import List

import numpy as np


def hash_key(key: List[str | int]) -> str:
    """Hash the given key array into sha1"""
    m = hashlib.sha1()
    for key_item in key:
        m.update(str(key_item).encode())
    return m.hexdigest()


def split_hash(h: str, mode: List[int]) -> List[str]:
    """Split hash into several key parts"""
    # mode must be strictly positive
    if any([m <= 0 for m in mode]):
        raise ValueError()

    # no hash given
    if not h:
        return []

    # we are using cumulated sum (cumsum) of modes from numpy
    return [f"_{h[:m]}" for m in np.cumsum(mode)] + [f"_{h}"]
