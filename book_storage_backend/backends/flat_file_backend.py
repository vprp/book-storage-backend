"""Module.

(c) Raise Partner 2022
Vincent Pfister
"""
import shutil
from pathlib import Path
from typing import Callable

import jsonpickle  # type: ignore[import]
from loguru import logger

from ..core.backend import (
    BookBackend,
    BookcaseBackend,
    LibraryBackend,
    ShelfBackend,
    StorageBackend,
)
from ..core.base_backend import BaseObjectBackend
from ..core.error import StorageError
from ..core.model import Book, Bookcase, Library, Shelf
from .base_flat_file_backend import BaseFlatFileItemsBackend, BaseFlatFileObjectBackend
from .file_tools import safe_create_dir
from .items_mapper import ItemsDirectoryMapper, ItemsFileMapper
from .key_encoder import IntKeyEncoder, StrKeyEncoder

LIBRARY_DATA_DIR = "library_data"


class FlatFileBookBackend(BaseFlatFileObjectBackend[Book], BookBackend):
    """Flat file implementation of the BookBackend."""

    def __init__(self, base_dir: Path):
        super().__init__(base_dir, Book)


class FlatFileShelfBackend(BaseFlatFileItemsBackend[Shelf, int, Book], ShelfBackend):
    """Flat file implementation of the ShelfBackend."""

    def __init__(self, directory: Path):
        mapper = ItemsFileMapper[int](directory, IntKeyEncoder("book", "json"))
        super().__init__(directory, Shelf, mapper)

    def _backend_factory(self, key: int) -> BaseObjectBackend[Book]:
        path = Path(self._mapper.path_for_key(key))
        return FlatFileBookBackend(path)


class FlatFileBookcaseBackend(
    BaseFlatFileItemsBackend[Bookcase, str, Shelf], BookcaseBackend
):
    """Flat-file implementation of BookcaseBackend."""

    def __init__(self, directory: Path):
        mapper = ItemsDirectoryMapper[str](directory, StrKeyEncoder("shelf"))
        super().__init__(directory, Bookcase, mapper)

    def _backend_factory(self, key: str) -> BaseObjectBackend[Shelf]:
        path = Path(self._mapper.path_for_key(key))
        return FlatFileShelfBackend(path)


class FlatFileLibraryBackend(
    BaseFlatFileItemsBackend[Library, int, Bookcase], LibraryBackend
):
    """Flat-file implementation of LibraryBackend."""

    def __init__(self, directory: Path):
        mapper = ItemsDirectoryMapper[int](directory, IntKeyEncoder("bookcase"))
        super().__init__(directory, Library, mapper)

    def _backend_factory(self, key: int) -> BaseObjectBackend[Bookcase]:
        path = Path(self._mapper.path_for_key(key))
        return FlatFileBookcaseBackend(path)


class FlatFileBackend(StorageBackend):
    """Flat-file implementation of the storage backend."""

    _storage_dir: Path

    def __init__(self, storage_dir: Path):
        self._storage_dir = storage_dir.joinpath(LIBRARY_DATA_DIR)

    def create(self) -> FlatFileLibraryBackend:
        safe_create_dir(self._storage_dir, "library storage")
        return FlatFileLibraryBackend(self._storage_dir)

    def get(self) -> FlatFileLibraryBackend:
        return FlatFileLibraryBackend(self._storage_dir)

    def clear(self):
        logger.info(f"clearing flat-file data ({self._storage_dir}).")
        shutil.rmtree(self._storage_dir, ignore_errors=True)

    def get_library(self, lib_factory: Callable[[], Library] = Library) -> Library:
        try:
            backend = self.get()
            logger.info("reading library at {path}", path=self._storage_dir)
            return backend.get_root()
        except StorageError:
            logger.info("creating library at {path}", path=self._storage_dir)
            backend = self.create()
            library = Library()
            backend.set_root(library)
            return library
