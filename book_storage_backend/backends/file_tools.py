"""Module with tools for the persistence to the file system.

`FileObjectStore[T]` handles the persistence of an object of type `T` to a JSON file.

`DataDir[T, K]` extends `FileObjectStore[T]` to add the persistence of children (with
keys of type `K`) to subdirectories.

`FileListDir[K, V]`

(c) Raise Partner 2022
Vincent Pfister
"""
import os
from abc import ABC, abstractmethod
from dataclasses import dataclass
from functools import cached_property
from json import JSONDecodeError
from pathlib import Path
from typing import Generic, Iterator, Optional, Type, TypeVar

import jsonpickle  # type: ignore[import]

from ..core.error import StorageError
from .key_encoder import KeyEncoder

DATA_JSON_FILE = "data.json"

T = TypeVar("T")
K = TypeVar("K")
V = TypeVar("V")
_X = TypeVar("_X")


@dataclass(frozen=True)
class DataInfo(Generic[_X, T]):
    data: _X
    object: T


class _ReaderWriter(Generic[T], ABC):
    """Interface for Reader/Writer"""

    @abstractmethod
    def write_data(self, obj: T, overwrite: bool = False) -> None:
        """Write the object to the data file.

        The object must be of type `self._klass`.
        Raises Storage error if the data file already exists, unless `overwrite`
        is True.
        """

    @abstractmethod
    def read_data(self) -> T:
        """Read the object from the data file.

        Raises StorageError if the file does not exist or cannot be read.
        Raises StorageError if the object cannot be loaded.
        Raises TypeError if the loaded object is not a `self._klass`.
        """


class FileObjectStore(Generic[T], _ReaderWriter[T]):
    """Manage the persistence of an object in a JSON file."""

    _klass: Type[T]
    _path: Path

    def __init__(self, path: Path, klass: Type[T]):
        """Constructor."""
        self._klass = klass
        self._path = check_data_dir_path(path)

    @classmethod
    def create_or_load(
        cls,
        data: _ReaderWriter[T],
        obj: Optional[T],
    ) -> T:
        """Class method for reading or creating a FileObjectStore

        If `obj` is given, the data file must not exist, and is created from `obj`.
        If `obj` is None, the data file must exist, and the object is read from it.
        Returns the object read or written (never None).
        """
        if obj is None:
            obj = data.read_data()
        else:
            data.write_data(obj)
        return obj

    @cached_property
    def data_file(self) -> Path:
        """Path of the json file containing the dump of the object."""
        return self._path.joinpath(DATA_JSON_FILE)

    def write_data(self, obj: T, overwrite: bool = False) -> None:
        """Write the object to the data file.

        The object must be of type `self._klass`.
        Raises Storage error if the data file already exists, unless `overwrite`
        is True.
        """
        if not isinstance(obj, self._klass):
            raise TypeError(f"obj '{obj}' must be of type '{self._klass}'")
        if not overwrite and self._data_file_is_valid:
            raise StorageError(f"cannot overwrite data file '{self.data_file}'")
        self.data_file.write_text(jsonpickle.encode(obj))

    def read_data(self) -> T:
        """Read the object from the data file.

        Raises StorageError if the file does not exist or cannot be read.
        Raises StorageError if the object cannot be loaded.
        Raises TypeError if the loaded object is not a `self._klass`.
        """
        if not (self.data_file.exists() and self.data_file.is_file()):
            raise StorageError(f"data file '{self.data_file}' not found")
        try:
            obj = jsonpickle.decode(self.data_file.read_text())
        except OSError as err:
            raise StorageError(f"cannot read data file '{self.data_file}'") from err
        except JSONDecodeError as err:
            raise StorageError(
                f"data file '{self.data_file}' is not valid JSON"
            ) from err
        if not isinstance(obj, self._klass):
            raise TypeError(
                f"object read from '{self.data_file}' has wrong type '{type(obj)}'"
                f", expected '{self._klass}'"
            )
        return obj

    @property
    def _data_file_is_valid(self) -> bool:
        """Returns True if the data file exists and is valid."""
        return self.data_file.exists() and self.data_file.is_file()


class DataDir(FileObjectStore[T], Generic[T, K]):
    """Directory-based generic implementation of the object and keys storage."""

    _key_encoder: KeyEncoder[K]

    def __init__(self, path: Path, klass: Type[T], key_encoder: KeyEncoder[K]):
        """Constructor."""
        super().__init__(path, klass)
        self._key_encoder = key_encoder

    @classmethod
    def create(
        cls,
        data_dir_path: Path,
        klass: Type[T],
        key_encoder: KeyEncoder[K],
        obj: Optional[T],
    ) -> "DataInfo[DataDir[T, K], T]":
        data_dir = DataDir(data_dir_path, klass, key_encoder)
        obj = FileObjectStore.create_or_load(data_dir, obj)
        return DataInfo(data_dir, obj)

    def keys(self) -> Iterator[K]:
        """Read the keys from the subdirectories having the right name pattern."""
        subs = [path.name for path in self._path.iterdir() if path.is_dir()]
        keys = [name for name in subs if self._key_encoder.is_key(name)]
        return (self._key_encoder.decode(k) for k in keys)

    def path_for_key(self, key: K) -> Path:
        return self._path.joinpath(self._key_encoder.encode(key))


class FileListDir(FileObjectStore[T], Generic[T, K, V]):

    _key_encoder: KeyEncoder[K]
    _item_klass: Type[V]
    _path: Path

    def __init__(
        self,
        path: Path,
        klass: Type[T],
        item_klass: Type[V],
        key_encoder: KeyEncoder[K],
    ):
        """Constructor."""
        super().__init__(path, klass)
        self._item_klass = item_klass
        self._key_encoder = key_encoder

    @classmethod
    def create(
        cls,
        data_dir_path: Path,
        klass: Type[T],
        item_klass: Type[V],
        key_encoder: KeyEncoder[K],
        obj: Optional[T],
    ) -> "DataInfo[FileListDir[T, K, V], T]":
        data = FileListDir(data_dir_path, klass, item_klass, key_encoder)
        obj = FileObjectStore.create_or_load(data, obj)
        return DataInfo(data, obj)

    def write_item_data(self, key: K, value: V, overwrite: bool = False) -> None:
        if value is None:
            raise ValueError("value cannot be None")
        if not isinstance(value, self._item_klass):
            raise TypeError(f"value '{value}' must be of type '{self._item_klass}'")

        path = self.path_for_key(key)
        if not overwrite and path.exists():
            raise KeyError(f"a value with key '{key}' already exists.")
        path.write_text(jsonpickle.encode(value))

    def read_item_data(self, key: K) -> V:
        try:
            return read_from_file(self.path_for_key(key), self._item_klass)
        except FileNotFoundError:
            raise KeyError(f"key '{key}' not found in {self}")

    def keys(self) -> Iterator[K]:
        """Read the keys from the files having the right name pattern."""
        subs = [path.name for path in self._path.iterdir() if path.is_file()]
        keys = [name for name in subs if self._key_encoder.is_key(name)]
        return (self._key_encoder.decode(k) for k in keys)

    def path_for_key(self, key: K) -> Path:
        return self._path.joinpath(self._key_encoder.encode(key))


def check_data_dir_path(path: Path) -> Path:
    """Checks if path is a valid data directory (raises if not), and returns it."""
    if not path.exists():
        raise FileNotFoundError(f"path '{path}' does not exist")
    if not path.is_dir():
        raise NotADirectoryError(f"path '{path}' is not a directory")
    if not _is_dir_writable(path):
        raise PermissionError(f"path '{path}' is not writable")
    return path


def _is_dir_writable(path: Path) -> bool:
    """Determine if a directory is writable by touching a temp file."""
    tmp_path = path.joinpath(".test_writable")
    writable = True
    try:
        tmp_path.touch()
    except PermissionError:
        writable = False
    finally:
        tmp_path.unlink(missing_ok=True)
    return writable


def read_from_file(data_file: Path, klass: Type[T]) -> T:
    """Read an object from the data file.

    Raises StorageError if the file does not exist or cannot be read.
    Raises StorageError if the object cannot be loaded.
    Raises TypeError if the loaded object is not a `klass`.
    """
    if not data_file.exists() and data_file.is_file():
        raise StorageError(f"data file '{data_file}' not found")
    try:
        obj = jsonpickle.decode(data_file.read_text())
    except FileNotFoundError:
        raise
    except OSError as err:
        raise StorageError(f"cannot read data file '{data_file}'") from err
    except JSONDecodeError as err:
        raise StorageError(f"data file '{data_file}' is not valid JSON") from err
    if not isinstance(obj, klass):
        raise TypeError(
            f"object read from '{data_file}' has wrong type '{type(obj)}'"
            f", expected '{klass}'"
        )
    return obj


def safe_create_dir(path: Path, message: str, exist_ok: bool = False):
    """Create a dir while catching relevant exceptions."""
    try:
        os.makedirs(path, exist_ok=exist_ok)
    except FileExistsError as err:
        raise KeyError(f"{message} already exists") from err
    except OSError as err:
        raise StorageError(f"Cannot create data directory for {message}") from err
