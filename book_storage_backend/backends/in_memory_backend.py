"""In-memory implementation for the model backends.

(c) Raise Partner 2022
Vincent Pfister
"""
from abc import ABC
from typing import (
    Callable,
    Generic,
    Hashable,
    Iterator,
    MutableMapping,
    Optional,
    TypeVar,
)

from book_storage_backend.core.backend import (
    BookBackend,
    BookcaseBackend,
    LibraryBackend,
    ShelfBackend,
    StorageBackend,
)
from book_storage_backend.core.base_backend import BaseItemsBackend, BaseObjectBackend
from book_storage_backend.core.error import StorageError
from book_storage_backend.core.model import Book, Bookcase, Library, Shelf
from book_storage_backend.core.storage import WithStorage

T = TypeVar("T")
TI = TypeVar("TI", bound=WithStorage)
KI = TypeVar("KI", bound=Hashable)
VI = TypeVar("VI")


class BaseInMemoryObjectBackend(BaseObjectBackend[T], Generic[T]):
    """In-memory generic implementation of the object backend."""

    _obj: Optional[T]

    def __init__(self):
        self._obj = None

    def _write_obj(self, value: T) -> None:
        if value is None:
            raise ValueError("value cannot be None")
        self._obj = value

    def _read_obj(self) -> T:
        if self._obj is None:
            raise StorageError("no object in store")
        return self._obj

    def _del_obj(self) -> None:
        self._obj = None


class BaseInMemoryItemsBackend(
    BaseInMemoryObjectBackend[TI],
    BaseItemsBackend[TI, KI, VI],
    Generic[TI, KI, VI],
    ABC,
):

    _items: MutableMapping[KI, BaseObjectBackend[VI]]

    def __init__(self):
        super().__init__()
        self._items = {}

    def keys(self) -> Iterator[KI]:
        yield from self._items.keys()


class InMemoryBookBackend(BookBackend, BaseInMemoryObjectBackend[Book]):
    pass


class InMemoryShelfBackend(ShelfBackend, BaseInMemoryItemsBackend[Shelf, int, Book]):
    def _backend_factory(self, key: int) -> BaseObjectBackend[Book]:
        try:
            return self._items[key]
        except KeyError:
            backend = self._items[key] = InMemoryBookBackend()
            return backend


class InMemoryBookcaseBackend(
    BookcaseBackend, BaseInMemoryItemsBackend[Bookcase, str, Shelf]
):
    def _backend_factory(self, key: str) -> BaseObjectBackend[Shelf]:
        try:
            return self._items[key]
        except KeyError:
            backend = self._items[key] = InMemoryShelfBackend()
            return backend


class InMemoryLibraryBackend(
    LibraryBackend, BaseInMemoryItemsBackend[Library, int, Bookcase]
):
    def _backend_factory(self, key: int) -> BaseObjectBackend[Bookcase]:
        try:
            return self._items[key]
        except KeyError:
            backend = self._items[key] = InMemoryBookcaseBackend()
            return backend


class InMemoryBackend(StorageBackend):
    """In-memory implementation of the storage backend."""

    __backend: Optional[InMemoryLibraryBackend]

    def clear(self):
        InMemoryBackend.__backend = None

    def get_library(self, lib_factory: Callable[[], Library] = Library) -> Library:
        if InMemoryBackend.__backend is None:
            backend = InMemoryLibraryBackend()
            library = lib_factory()
            backend.set_root(library)
            InMemoryBackend.__backend = backend
            return library
        else:
            return InMemoryBackend.__backend.get_root()
