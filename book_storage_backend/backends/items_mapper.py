"""Module defining item mappers.

(c) Raise Partner 2022
Vincent Pfister
"""
from abc import ABC, abstractmethod
from pathlib import Path
from typing import Generic, Iterator, TypeVar

from book_storage_backend.backends.key_encoder import KeyEncoder

K = TypeVar("K")


class ItemsMapper(Generic[K], ABC):
    """Mapper between items persisted on disc and keys."""

    _key_encoder: KeyEncoder[K]
    _base_path: Path

    def __init__(self, base_path: Path, key_encoder: KeyEncoder[K]):
        """Constructor."""
        self._base_path = base_path
        self._key_encoder = key_encoder

    @abstractmethod
    def keys(self) -> Iterator[K]:
        """Iterator on the keys of this item collection."""

    @abstractmethod
    def path_for_key(self, key: K) -> Path | str:
        """Form the item path for the given key."""

    @staticmethod
    def _empty_keys() -> Iterator[K]:
        yield from ()


class ItemsDirectoryMapper(ItemsMapper[K], Generic[K]):
    """ItemsMapper using subdirectories."""

    def keys(self) -> Iterator[K]:
        """Read the keys from the subdirectories having the right name pattern."""
        if not self._base_path.is_dir():
            return self._empty_keys()
        subs = [path.name for path in self._base_path.iterdir() if path.is_dir()]
        keys = [name for name in subs if self._key_encoder.is_key(name)]
        return (self._key_encoder.decode(k) for k in keys)

    def path_for_key(self, key: K) -> Path:
        return self._base_path.joinpath(self._key_encoder.encode(key))


class ItemsFileMapper(ItemsMapper[K], Generic[K]):
    """ItemsMapper using files in a directory."""

    def keys(self) -> Iterator[K]:
        """Read the keys from the files having the right name pattern."""
        if not self._base_path.is_dir():
            return self._empty_keys()
        subs = [path.name for path in self._base_path.iterdir() if path.is_file()]
        keys = [name for name in subs if self._key_encoder.is_key(name)]
        return (self._key_encoder.decode(k) for k in keys)

    def path_for_key(self, key: K) -> Path:
        return self._base_path.joinpath(self._key_encoder.encode(key))
