"""Module.

(c) Raise Partner 2022
Vincent Pfister
"""
from typing import Generic, Type, TypeVar

K = TypeVar("K")  # , int, str


class KeyEncoder(Generic[K]):
    """Converter between a key of type `K` and a key name (file name, ...)."""

    klass: Type[K]

    def __init__(self, klass: Type[K], prefix: str = "", suffix=""):
        if len(prefix) < 1:
            raise ValueError("prefix is too short")
        if prefix.startswith("."):
            raise ValueError("prefix cannot start with a dot")
        self.prefix = prefix + "_"
        self.suffix = "." + suffix if suffix else ""
        self.klass = klass

    def encode(self, key: K) -> str:
        return self.prefix + self._encode_key(key) + self.suffix

    def _encode_key(self, key: K) -> str:
        return str(key)

    def decode(self, s: str) -> K:
        return self._decode_key(self._strip(s))

    def _decode_key(self, s: str) -> K:
        try:
            return self.klass(s)  # type: ignore
        except TypeError:
            raise NotImplementedError(
                "KeyEncoder[K]._decode must be overriden: "
                "K has no constructor taking a string"
            )

    def is_key(self, s: str) -> bool:
        return s.startswith(self.prefix) and s.endswith(self.suffix)

    def _strip(self, s: str) -> str:
        start = len(self.prefix)
        end = len(s) - len(self.suffix)
        return s[start:end]


class IntKeyEncoder(KeyEncoder[int]):
    def __init__(self, prefix: str = "", suffix: str = ""):
        super().__init__(int, prefix, suffix)

    def _encode_key(self, key: int) -> str:
        return f"{key:03}"


class StrKeyEncoder(KeyEncoder[str]):
    def __init__(self, prefix: str = "", suffix: str = ""):
        super().__init__(str, prefix, suffix)
