"""Module implemenmting thd HDF storage backend.

- the library properties are stored in a data.json file
- Each bookcase is stored in a separate h5 file.
- In the file, the shelf is stored in a group.
- The books are stored in a dataframe with a single row, in the shelf subgroup

(c) Raise Partner 2022
Vincent Pfister
"""
import shutil
from abc import ABC
from pathlib import Path
from typing import Callable, Generic, Hashable, Iterator, Tuple, Type, TypeVar, Dict, Any

import jsonpickle  # type: ignore[import]
import pandas as pd
import tables  # type: ignore[import]

from ..core.backend import (
    BookBackend,
    BookcaseBackend,
    LibraryBackend,
    ShelfBackend,
    StorageBackend,
)
from ..core.base_backend import BaseItemsBackend, BaseObjectBackend
from ..core.error import StorageError
from ..core.model import Book, Bookcase, Library, Shelf
from ..core.storage import WithStorage
from .base_flat_file_backend import BaseFlatFileItemsBackend
from .file_tools import safe_create_dir
from .items_mapper import ItemsFileMapper, ItemsMapper
from .key_encoder import IntKeyEncoder, KeyEncoder, StrKeyEncoder

LIBRARY_DATA_DIR = "library_data"
K = TypeVar("K")


class HdfGroupItemsMapper(ItemsMapper[K], Generic[K]):
    """ItemsMapper using HDF subgroups."""

    _data_path: str

    def __init__(self, hdf_file: Path, key_encoder: KeyEncoder[K], data_path: str = ""):
        """Constructor."""
        super().__init__(hdf_file, key_encoder)
        self._data_path = self._normalize_data_path(data_path)

    @staticmethod
    def _normalize_data_path(data_path: str) -> str:
        s = data_path if data_path.startswith("/") else f"/{data_path}"
        return s[:-1] if s.endswith("/") else s

    def keys(self) -> Iterator[K]:
        if not self._base_path.is_file():
            return self._empty_keys()
        with pd.HDFStore(self._base_path, "r") as ds:
            subgroups = ds.get_node(self._data_path) or ()  # type: ignore[operator]
            for subgroup in subgroups:
                # noinspection PyProtectedMember
                name = str(subgroup._v_name)
                if self._key_encoder.is_key(name):
                    yield self._key_encoder.decode(name)

    def path_for_key(self, key: K) -> str:
        return f"{self._data_path}/{self._key_encoder.encode(key)}"


T = TypeVar("T")
TI = TypeVar("TI", bound=WithStorage)
KI = TypeVar("KI", bound=Hashable)
VI = TypeVar("VI")


def split_data_path(path: str) -> Tuple[str, str]:
    p = Path(path if path.startswith("/") else f"/{path}")
    return str(p.parent), p.name


class BaseHdfObjectBackend(BaseObjectBackend[T], Generic[T]):
    """HDF generic implementation of the _object_ backend."""

    _hdf_file: Path
    _klass: Type[T]
    _data_path: str

    def __init__(self, hdf_file: Path, klass: Type[T], data_path: str) -> None:
        """Constructor."""
        self._hdf_file = hdf_file
        self._klass = klass
        self._data_path = data_path

    def _write_obj(self, value: T) -> None:
        if not isinstance(value, self._klass):
            raise TypeError(f"value '{value}' must be of type '{self._klass}'")
        with pd.HDFStore(self._hdf_file, "a") as ds:
            node = ds.get_node(self._data_path)  # type: ignore[operator]
            if node is None:
                file: tables.File = ds.root._v_file
                parent, name = split_data_path(self._data_path)
                node = file.create_group(parent, name, createparents=True)
            node._v_attrs.data = jsonpickle.encode(value)

    def _read_obj(self) -> T:
        with pd.HDFStore(self._hdf_file, "r") as ds:
            node = ds.get_node(self._data_path)  # type: ignore[operator]
            if node is None:
                raise StorageError(
                    f"the data file '{self._hdf_file}' "
                    f"has no item '{self._data_path}'"
                )
            obj = jsonpickle.decode(node._v_attrs.data)
            if not isinstance(obj, self._klass):
                raise TypeError(
                    f"object '{self._data_path}' read from '{self._hdf_file}' "
                    f"has wrong type '{type(obj)}', expected '{self._klass}'"
                )
            return obj

    def _del_obj(self) -> None:
        if not self._hdf_file.is_file():
            raise StorageError(f"HDF storage file '{self._hdf_file}' not found")
        with pd.HDFStore(self._hdf_file, "a") as ds:
            node = ds.get_node(self._data_path)  # type: ignore[operator]
            if node is not None:
                file: tables.File = ds.root._v_file
                where = "/".join(split_data_path(self._data_path))
                file.remove_node(where, recursive=True)


class BaseHdfItemsBackend(
    BaseHdfObjectBackend[TI], BaseItemsBackend[TI, KI, VI], Generic[TI, KI, VI], ABC
):
    """HDF generic implementation of the _items_ backend."""

    _mapper: ItemsMapper[KI]

    def __init__(
        self, hdf_file: Path, klass: Type[TI], mapper: ItemsMapper[KI], data_path: str
    ):
        """Constructor."""
        super().__init__(hdf_file, klass, data_path)
        self._mapper = mapper

    def keys(self) -> Iterator[KI]:
        yield from self._mapper.keys()


class HdfBookBackend(BaseHdfObjectBackend[Book], BookBackend):
    """Hdf implementation of BookBackend."""

    def __init__(self, hdf_file: Path, data_path: str):
        """Constructor."""
        super().__init__(hdf_file, Book, data_path)

    def _write_obj(self, value: Book) -> None:
        if not isinstance(value, self._klass):
            raise TypeError(f"value '{value}' must be of type '{self._klass}'")
        with pd.HDFStore(self._hdf_file, "a") as ds:
            ds[self._data_path] = pd.DataFrame([value])

    def _read_obj(self) -> Book:
        with pd.HDFStore(self._hdf_file, "r") as ds:
            book_values: Dict[str, Any] = ds[self._data_path].iloc[0, :].to_dict()  # type: ignore[call-overload, assignment]
            return Book(**book_values)


class HdfShelfBackend(BaseHdfItemsBackend[Shelf, int, Book], ShelfBackend):
    """Hdf implementation of ShelfBackend."""

    def __init__(self, hdf_file: Path, data_path: str):
        """Constructor."""
        mapper = HdfGroupItemsMapper[int](hdf_file, IntKeyEncoder("book"), data_path)
        super().__init__(hdf_file, Shelf, mapper, data_path)

    def _backend_factory(self, key: int) -> BaseObjectBackend[Book]:
        path = str(self._mapper.path_for_key(key))
        return HdfBookBackend(self._hdf_file, path)


class HdfBookcaseBackend(BaseHdfItemsBackend[Bookcase, str, Shelf], BookcaseBackend):
    """Hdf implementation of BookcaseBackend."""

    def __init__(
        self,
        hdf_file: Path,
    ) -> None:
        mapper = HdfGroupItemsMapper[str](hdf_file, StrKeyEncoder("shelf"))
        super().__init__(hdf_file, Bookcase, mapper, "/")

    def _backend_factory(self, key: str) -> BaseObjectBackend[Shelf]:
        path = str(self._mapper.path_for_key(key))
        return HdfShelfBackend(self._hdf_file, path)


class HdfLibraryBackend(
    BaseFlatFileItemsBackend[Library, int, Bookcase], LibraryBackend
):
    """HDF implementation of the library backend."""

    def __init__(self, base_dir: Path) -> None:
        """ "Constructor."""
        mapper = ItemsFileMapper[int](base_dir, IntKeyEncoder("bookcase", "h5"))
        super().__init__(base_dir, Library, mapper)

    def keys(self) -> Iterator[int]:
        yield from self._mapper.keys()

    def _backend_factory(self, key: int) -> HdfBookcaseBackend:
        path = Path(self._mapper.path_for_key(key))
        return HdfBookcaseBackend(path)


class HdfBackend(StorageBackend):
    """HDF implementation of the storage backend."""

    _storage_dir: Path

    def __init__(self, storage_dir: Path):
        self._storage_dir = storage_dir.joinpath(LIBRARY_DATA_DIR)

    def create(self) -> HdfLibraryBackend:
        safe_create_dir(self._storage_dir, "library storage")
        return HdfLibraryBackend(self._storage_dir)

    def get(self) -> HdfLibraryBackend:
        return HdfLibraryBackend(self._storage_dir)

    def clear(self):
        shutil.rmtree(self._storage_dir, ignore_errors=True)

    def get_library(self, lib_factory: Callable[[], Library] = Library) -> Library:
        try:
            backend = self.get()
            return backend.get_root()
        except StorageError:
            backend = self.create()
            library = Library()
            backend.set_root(library)
            return library
