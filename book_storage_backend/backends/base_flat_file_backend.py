"""Module.

(c) Raise Partner 2022
Vincent Pfister
"""
from abc import ABC
from pathlib import Path
from typing import Generic, Hashable, Iterator, Type, TypeVar

import jsonpickle  # type: ignore[import]

from book_storage_backend.backends.file_tools import read_from_file
from book_storage_backend.backends.items_mapper import ItemsMapper
from book_storage_backend.core.base_backend import BaseItemsBackend, BaseObjectBackend
from book_storage_backend.core.error import StorageError
from book_storage_backend.core.storage import WithStorage

T = TypeVar("T")
TI = TypeVar("TI", bound=WithStorage)
KI = TypeVar("KI", bound=Hashable)
VI = TypeVar("VI")


class BaseFlatFileObjectBackend(BaseObjectBackend[T], Generic[T]):
    """Flat file generic implementation of the _object_ backend.

    The object is stored in a JSON file."""

    _path: Path
    _klass: Type[T]

    def __init__(self, path: Path, klass: Type[T]):
        """Constructor."""
        self._path = path
        self._klass = klass

    def _write_obj(self, value: T) -> None:
        if not isinstance(value, self._klass):
            raise TypeError(f"value '{value}' must be of type '{self._klass}'")
        self._path.parent.mkdir(exist_ok=True)
        self._path.write_text(jsonpickle.encode(value))

    def _read_obj(self) -> T:
        try:
            return read_from_file(self._path, self._klass)
        except FileNotFoundError as err:
            raise StorageError(f"object file '{self._path}' not found") from err

    def _del_obj(self) -> None:
        try:
            self._path.unlink()
        except FileNotFoundError as err:
            raise StorageError(f"Storage file '{self._path}' not found") from err


class BaseFlatFileItemsBackend(
    BaseFlatFileObjectBackend[TI],
    BaseItemsBackend[TI, KI, VI],
    Generic[TI, KI, VI],
    ABC,
):
    """Flat file generic implementation of the _items_ backend."""

    _mapper: ItemsMapper[KI]

    def __init__(self, directory: Path, klass: Type[TI], mapper: ItemsMapper[KI]):
        """Constructor."""
        super().__init__(directory.joinpath("data.json"), klass)
        self._mapper = mapper

    def keys(self) -> Iterator[KI]:
        yield from self._mapper.keys()
