import random
from dataclasses import dataclass
from enum import Enum
from pathlib import Path
from typing import Iterator

import click

from book_storage_backend.backends.flat_file_backend import FlatFileBackend
from book_storage_backend.backends.hdf_backend import HdfBackend
from book_storage_backend.backends.in_memory_backend import InMemoryBackend
from book_storage_backend.core.model import Book, Bookcase, Library, Shelf
from book_storage_backend.utils.phrases import random_phrase


class SetupMode(Enum):
    SINGLE = "single"
    RANDOM = "random"


class BackendMode(Enum):
    IM_MEMORY = "in-memory"
    FLAT_FILE = "flat-file"
    HDF = "hdf"


def get_data_path() -> Path:
    """Return the data path, creating it if needed."""
    path = Path.cwd().joinpath("data")
    path.mkdir(exist_ok=True)
    return path


def get_library(mode: BackendMode = BackendMode.IM_MEMORY) -> Library:
    """Make a library with a backend corresponding to the mode."""
    library: Library
    match mode:
        case BackendMode.IM_MEMORY:
            library = InMemoryBackend().get_library()
        case BackendMode.FLAT_FILE:
            library = FlatFileBackend(get_data_path()).get_library()
        case BackendMode.HDF:
            library = HdfBackend(get_data_path()).get_library()
        case _:
            raise NotImplementedError(f"backend mode '{mode}' is not implemented")
    return library


def clear_library_backend(mode: BackendMode = BackendMode.IM_MEMORY) -> None:
    match mode:
        case BackendMode.FLAT_FILE:
            FlatFileBackend(get_data_path()).clear()


def setup_library(library: Library, mode: SetupMode = SetupMode.SINGLE) -> None:
    match mode:

        case SetupMode.SINGLE:
            bookcase = library.bookcases.add(999, Bookcase())
            shelf = bookcase.shelves.add("zzz", Shelf())
            shelf.books.add(888, Book(777, "title", "author"))

        case SetupMode.RANDOM:
            for aisle in (1, 2, 3):
                bookcase = library.bookcases.add(aisle, Bookcase())
                for shelf_row in list("ABC"):
                    shelf = bookcase.shelves.add(shelf_row, Shelf())
                    for book in make_books(2):
                        shelf.books.add(book.book_id, book)


def make_books(count: int) -> Iterator[Book]:
    for _ in range(count):
        book_id = random.randrange(100, 1000)
        title = random_phrase(4)
        author = random_phrase(2)
        yield Book(book_id, title, author)


class EnumType(click.Choice):
    def __init__(self, enum, case_sensitive=False):
        self.__enum = enum
        super().__init__(
            choices=[item.value for item in enum], case_sensitive=case_sensitive
        )

    def convert(self, value, param, ctx):
        if isinstance(value, self.__enum):
            converted = value
        else:
            converted_str = super().convert(value, param, ctx)
            converted = self.__enum(converted_str)
        return converted


@dataclass
class CommandOptions:
    backend_mode: BackendMode = BackendMode.IM_MEMORY


@click.group()
@click.option(
    "--backend-mode", "-b", default=BackendMode.IM_MEMORY, type=EnumType(BackendMode)
)
@click.pass_context
def cli(ctx: click.Context, backend_mode: BackendMode):
    """Base command."""
    ctx.ensure_object(CommandOptions)
    ctx.obj.backend_mode = backend_mode


@cli.command()
@click.option(
    "--library-setup", "-L", default=SetupMode.SINGLE, type=EnumType(SetupMode)
)
@click.pass_context
def setup(ctx, library_setup: SetupMode):
    opt: CommandOptions = ctx.obj
    library = get_library(opt.backend_mode)
    setup_library(library, library_setup)
    library.show_catalog()


@cli.command()
@click.pass_context
def clear(ctx):
    opt: CommandOptions = ctx.obj
    clear_library_backend(opt.backend_mode)


@cli.command()
@click.pass_context
def show(ctx):
    opt: CommandOptions = ctx.obj
    get_library(opt.backend_mode).show_catalog()
