"""Module.

(c) Raise Partner 2022
Vincent Pfister
"""


class StorageError(Exception):
    """Generic storage exception class."""

    pass
