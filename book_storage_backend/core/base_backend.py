"""Module defining the abstract backend classes supporting the model storage.


This module defines two types of backends:
- **object backends** derive from `BaseObjectBackend`, and do not have a child
  key/value store.
  They only read, write and delete the value of a target object for persistence
- **items backends** derive from `BaseItemsBackend`, and add a child key/value
  store to the object store.
  This backend provides add, get, set, delete actions on items.

The generic _TypeVar_ used are:
- `T`: Type of the object stored in the "object" backend
- `TI`: Type of the object stored in the "items" backend (must be a `WithStorage`)
- `KI`: Type of the keys of the items (must be hashable)

(c) Raise Partner 2022
Vincent Pfister
"""
from abc import ABC, abstractmethod
from typing import Generic, Hashable, Iterator, TypeVar

from book_storage_backend.core.storage import Storage, WithStorage

T = TypeVar("T")

TI = TypeVar("TI", bound=WithStorage)
KI = TypeVar("KI", bound=Hashable)
VI = TypeVar("VI")


class BaseObjectBackend(Generic[T], ABC):
    """Generic abstract base class for object backends."""

    @abstractmethod
    def _write_obj(self, value: T) -> None:
        pass

    @abstractmethod
    def _read_obj(self) -> T:
        pass

    @abstractmethod
    def _del_obj(self) -> None:
        pass

    def _register_value(self, value: T) -> None:
        """Register this backend with the given value. Does nothing by default."""
        pass


class BaseItemsBackend(
    BaseObjectBackend[TI], Storage[KI, VI], Generic[TI, KI, VI], ABC
):
    """Generic abstract base class for items backends."""

    @abstractmethod
    def _backend_factory(self, key: KI) -> BaseObjectBackend[VI]:
        pass

    def _has_key(self, key: KI) -> bool:
        """Return True if this ItemsBackend has an item with the given key."""
        return any(key == k for k in self.keys())

    def _add_item(self, key: KI, value: VI) -> None:
        """Called after the object of an item has been added.

        Override if action is required here."""
        pass

    def _del_item(self, key: KI) -> None:
        """Called after the object of an item has been deleted.

        Override if cleanup is required here."""
        pass

    def _register_value(self, value: TI) -> None:
        """Register this ItemsBackend with the given value."""
        try:
            value.use_storage(self)
        except AttributeError as err:
            raise TypeError(
                f"cannot use backend '{type(self)}' with items "
                f"of type {type(value)}: missing `WithStorage` base class"
            ) from err

    def _check_key_exists(self, key, should_exist: bool = True):
        if should_exist and not self._has_key(key):
            raise KeyError(f"there is no item with key '{key}'")
        if not should_exist and self._has_key(key):
            raise KeyError(f"an item with key '{key}' already exists")

    @abstractmethod
    def keys(self) -> Iterator[KI]:
        pass

    def add(self, key: KI, value: VI) -> VI:
        self._check_key_exists(key, should_exist=False)
        backend = self._backend_factory(key)
        backend._write_obj(value)
        backend._register_value(value)
        self._add_item(key, value)
        return value

    def get(self, key: KI) -> VI:
        self._check_key_exists(key)
        backend = self._backend_factory(key)
        value = backend._read_obj()
        backend._register_value(value)
        return value

    def set(self, key: KI, value: VI) -> VI:
        self._check_key_exists(key)
        backend = self._backend_factory(key)
        backend._write_obj(value)
        backend._register_value(value)
        return value

    def delete(self, key: KI) -> None:
        self._check_key_exists(key)
        backend = self._backend_factory(key)
        backend._del_obj()
        self._del_item(key)


class BaseRootBackend(BaseItemsBackend[TI, KI, VI], Generic[TI, KI, VI], ABC):
    """Generic abstract base class for the root backend."""

    def set_root(self, value: TI) -> None:
        """Set the root object of the backend."""
        self._write_obj(value)
        self._register_value(value)

    def get_root(self) -> TI:
        """Get the root object of the backend."""
        value = self._read_obj()
        self._register_value(value)
        return value
