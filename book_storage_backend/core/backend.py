"""Module defining the abstract backend classes supporting the model storage.

(c) Raise Partner 2022
Vincent Pfister
"""
from abc import ABC, abstractmethod
from typing import Callable

from book_storage_backend.core.base_backend import (
    BaseItemsBackend,
    BaseObjectBackend,
    BaseRootBackend,
)
from book_storage_backend.core.model import Book, Bookcase, Library, Shelf


class BookBackend(BaseObjectBackend[Book], ABC):
    """Interface for a book backend."""


class ShelfBackend(BaseItemsBackend[Shelf, int, Book], ABC):
    """Interface for a shelf backend."""


class BookcaseBackend(BaseItemsBackend[Bookcase, str, Shelf], ABC):
    """Interface for a bookcase backend."""


class LibraryBackend(BaseRootBackend[Library, int, Bookcase], ABC):
    """Interface for a library backend."""


class StorageBackend(ABC):
    """Interface for the storage backends of library, bookcase and shelf."""

    @abstractmethod
    def clear(self):
        pass

    @abstractmethod
    def get_library(self, lib_factory: Callable[[], Library] = Library) -> Library:
        pass
