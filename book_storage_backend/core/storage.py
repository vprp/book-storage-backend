"""Definition of the Storage interface.

(c) Raise Partner 2022
Vincent Pfister
"""
from abc import ABC, abstractmethod
from typing import Generic, Iterator, Optional, TypeVar

K_ITEM = TypeVar("K_ITEM")
V_ITEM = TypeVar("V_ITEM")


class Storage(Generic[K_ITEM, V_ITEM], ABC):
    """Storage[key, value] interface.

    The key type K must be hashable.
    """

    @abstractmethod
    def add(self, key: K_ITEM, value: V_ITEM) -> V_ITEM:
        """Add a new value to the storage for the given key.

        :param key: must be unique in the storage
        :param value: item referenced by the key
        :return: the inserted value (useful for chaining)
        :raises: KeyError: if the key already exists
        """
        pass

    @abstractmethod
    def get(self, key: K_ITEM) -> V_ITEM:
        """Get an existing value from the storage by its key

        :param key: key of the item we want to access
        :return: the item
        :raises: KeyError: if the key does not exist
        """
        pass

    @abstractmethod
    def keys(self) -> Iterator[K_ITEM]:
        """Iterator on the keys present in the storage.

        :return: iterator on keys
        """
        pass


class WithStorage(Generic[K_ITEM, V_ITEM]):
    """Base class used to add support for a storage to a model.

    The storage implementation must be set after creation with a call to
    `use_storage(...)`.
    """

    __storage: Optional[Storage[K_ITEM, V_ITEM]] = None

    @property
    def _storage(self) -> Storage[K_ITEM, V_ITEM]:
        if self.__storage is None:
            raise AttributeError(f"The storage is not set for '{self}'")
        return self.__storage

    def use_storage(self, storage: Storage[K_ITEM, V_ITEM]) -> None:
        """Sets the storage implementation to be used with this object.

        You should most likely NOT call this method outside of the implementation
        of a storage backend.
        """
        self.__storage = storage
