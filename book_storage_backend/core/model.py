"""Module defining the domain model classes.

(c) Raise Partner 2022
Vincent Pfister
"""
from dataclasses import dataclass

from book_storage_backend.core.storage import Storage, WithStorage


@dataclass
class Book:
    """Represent a book."""

    book_id: int
    title: str
    author: str
    width: float = 3
    height: float = 20


class Shelf(WithStorage[int, Book]):
    """Represent a shelf having books."""

    _width: float

    def __init__(self, width: int = 20):
        """Constructor."""
        super().__init__()
        self._width = width

    @property
    def books(self) -> Storage[int, Book]:
        """Access to the books on the shelf."""
        return self._storage

    def _set_books(self, books: Storage[int, Book]) -> None:
        self._books = books

    def __eq__(self, other):
        """Override to provide shallow equality: the `books` content is not compared."""
        if not isinstance(other, Shelf):
            return False
        return self._width == other._width


@dataclass
class Bookcase(WithStorage[str, Shelf]):
    """Represents a bookcase having shelves."""

    height: float = 50

    def __init__(self, height: float = 50) -> None:
        """Constructor."""
        super().__init__()
        self._height = height

    @property
    def shelves(self) -> Storage[str, Shelf]:
        """Access to the shelves of the bookcase."""
        return self._storage


class Library(WithStorage[int, Bookcase]):
    """Represents a library having bookcases."""

    def __init__(self):
        """Constructor."""
        super().__init__()

    @property
    def bookcases(self) -> Storage[int, Bookcase]:
        """Access to the bookcases of the library"""
        return self._storage

    def show_catalog(self) -> None:
        """Display the content of the library on the console."""
        indent = "    "
        for aisle in self.bookcases.keys():
            bookcase = self.bookcases.get(aisle)
            print(0 * indent + f"bookcase: aisle {aisle} ({bookcase})")
            for row in bookcase.shelves.keys():
                shelf = bookcase.shelves.get(row)
                print(1 * indent + f"shelf: row {row} ({shelf})")
                for book_id in shelf.books.keys():
                    book = shelf.books.get(book_id)
                    print(2 * indent + f"{book_id:03}: {book.title} ({book.author})")
